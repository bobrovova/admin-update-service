const hydra     = require('./hydra');
const socketIO  = require('socket.io');
const http      = require('http');
const server    = http.createServer();

/**
 * AdminUpdateService is handler update-balance signal. It proccess signal and emit socket action for front-end
 */
class AdminUpdateService {
    constructor(){
        server.listen(4233);
        this.io = socketIO();
        this.io.attach(server);

        hydra.ready()
            .then(this.socketConnectionHandler.bind(this));
    }

    /**
     * Initialize listener of socket connection
     */
    socketConnectionHandler(){
        this.io.on('connection', function(socket){
            this.socket = socket;
            console.log("Socket connected: " + socket.id);
            this.hydraMessageHandler();
        }.bind(this));
    }

    /**
     * Handle incoming hydra-messages and emit some actions which depend on type of message
     */
    hydraMessageHandler(){
        hydra.registerService();
        hydra.on('message', function(message){
            if(message.typ == 'update-balance'){
                this.socket.emit('action', {
                    type: 'message',
                    data: 'update-balance'
                });
            }
        }.bind(this));
    }
}

const adminUpdateService = new AdminUpdateService();

module.exports = AdminUpdateService;
